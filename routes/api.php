<?php

use App\Http\Controllers\api\ActivationAccountController;
use App\Http\Controllers\api\AuthController;
use App\Http\Controllers\api\ForgotPasswordController;
use App\Http\Controllers\api\OrderController;
use App\Http\Controllers\api\ProfileController;
use Illuminate\Support\Facades\Route;

// Auth
Route::post('/register', [AuthController::class, 'registration']);
Route::post('/login', [AuthController::class, 'authenticate']);
Route::post('/forgot-password', [ForgotPasswordController::class, 'forgot']);

// Verification Account
Route::post('/send-otp', [ActivationAccountController::class, 'sendOtp']);
Route::post('/verified-otp', [ActivationAccountController::class, 'verifiedOtp']);

Route::middleware('auth:sanctum')->group(function () {
    // Profile
    Route::get('/profile', [ProfileController::class, 'profile']);
    Route::post('/change-password', [ProfileController::class, 'changePassword']);
    Route::post('/change-picture', [ProfileController::class, 'changePicture']);
    Route::post('/change-status', [ProfileController::class, 'changeStatus']);

    // Order History
    Route::get('order/earnings', [OrderController::class, 'earnings']);
    Route::get('order/history', [OrderController::class, 'index']);
    Route::get('order/{shipment:number}', [OrderController::class, 'show']);
    // Order Accept
    Route::post('order/accept/{shipment:number}', [OrderController::class, 'acceptOrder']);
    // Order Cancelled
    Route::post('order/cancel/{shipment:number}', [OrderController::class, 'cancelOrder']);
    // Order Completed
    Route::post('order/completed/{shipment:number}', [OrderController::class, 'completedOrder']);

    // Logout
    Route::delete('/logout', [AuthController::class, 'logout']);
});
