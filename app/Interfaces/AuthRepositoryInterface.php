<?php

namespace App\Interfaces;

interface AuthRepositoryInterface
{
    public function cekCrenditialsCourier(array $courierDetails);
    public function getCourier();
    public function getCourierByAuth();
    public function getCourierByEmail($email);
    public function getToken();
    public function registration(array $courierDetails);
    public function logout();
}
