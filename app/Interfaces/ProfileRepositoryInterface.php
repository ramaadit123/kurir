<?php

namespace App\Interfaces;

interface ProfileRepositoryInterface
{
    public function getProfile();
    public function updatePassword(array $password);
    public function updatePicture(array $image);
    public function updateStatus($status);
}
