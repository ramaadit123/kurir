<?php

namespace App\Interfaces;

interface ForgotPasswordRepositoryInterface
{
    public function getCourierByEmail($email);
    public function getToken($email, $token);
    public function updatePassword($request);
}
