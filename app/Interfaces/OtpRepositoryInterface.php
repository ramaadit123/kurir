<?php

namespace App\Interfaces;

interface OtpRepositoryInterface
{
    public function getAccountByEmail($email);
    public function getOtp($otp);
    public function updateStatus($request);
    public function verifToken($email, $token, $time);
}
