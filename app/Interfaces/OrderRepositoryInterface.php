<?php

namespace App\Interfaces;

interface OrderRepositoryInterface
{
    public function getHistory();
    public function acceptOrder($shipment);
    public function cancelOrder($shipment);
    public function completedOrder($shipment, array $data);
}
