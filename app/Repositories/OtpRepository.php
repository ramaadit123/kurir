<?php

namespace App\Repositories;

use App\Interfaces\OtpRepositoryInterface;
use App\Models\Courier;
use App\Models\EmailVerification;
use App\Models\PasswordReset;

class OtpRepository implements OtpRepositoryInterface
{
    public function getAccountByEmail($email)
    {
        return Courier::where('email', $email)->first();
    }

    public function getOtp($otp)
    {
        return EmailVerification::where('otp', $otp)->first();
    }

    public function updateStatus($request)
    {
        return Courier::where('id', $request->id)->update([
            'status' => '1',
        ]);
    }

    public function verifToken($email, $token, $time)
    {
        PasswordReset::updateOrCreate(
            ['email' => $email],
            [
                'email' => $email,
                'token' => $token,
                'time' => $time
            ]
        );
    }
}
