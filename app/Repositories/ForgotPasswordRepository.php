<?php

namespace App\Repositories;

use App\Interfaces\ForgotPasswordRepositoryInterface;
use App\Models\Courier;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Auth;

class ForgotPasswordRepository implements ForgotPasswordRepositoryInterface
{
    public function getCourierByEmail($email)
    {
        return Courier::where('email', $email)->first();
    }

    public function getToken($email, $token)
    {
        return PasswordReset::where('email', $email)->where('token', $token)->first();
    }

    public function updatePassword($request)
    {
        Courier::where('email', $request->email)->update([
            'password' => bcrypt($request->new_password)
        ]);
        $token = $this->getToken($request->email, $request->token);
        return $token->delete();
    }
}
