<?php

namespace App\Repositories;

use App\Interfaces\AuthRepositoryInterface;
use App\Models\Courier;
use Illuminate\Support\Facades\Auth;

class AuthRepository implements AuthRepositoryInterface
{
    public function cekCrenditialsCourier(array $courierDetails)
    {
        if (Auth::attempt($courierDetails)) {
            return true;
        }
        return false;
    }

    public function getCourier()
    {
        return Courier::get();
    }

    public function getCourierByAuth()
    {
        return Courier::where('id', Auth::user()->id)->first();
    }

    public function getCourierByEmail($email)
    {
        return Courier::where('id', Auth::user()->id)->where('email', $email)->first();
    }

    public function getToken()
    {
        $data = Auth::user();
        $data['token'] = $data->createToken('auth_token')->plainTextToken;
        return $data;
    }

    public function registration(array $courierDetails)
    {
        return Courier::create($courierDetails);
    }

    public function logout()
    {
        return Auth::user()->tokens()->delete();
    }
}
