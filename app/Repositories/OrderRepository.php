<?php

namespace App\Repositories;

use App\Interfaces\OrderRepositoryInterface;
use App\Models\CancelShipment;
use App\Models\Shipment;
use Illuminate\Support\Facades\Auth;

class OrderRepository implements OrderRepositoryInterface
{
    public function getHistory()
    {
        return Shipment::where('courier_id', Auth::user()->id);
    }

    public function acceptOrder($shipment)
    {
        return $shipment->update(['courier_id' => Auth::user()->id, 'status' => 'Menunggu Pengambilan']);
    }

    public function cancelOrder($shipment)
    {
        CancelShipment::create([
            'shipment_id' => $shipment->id,
            'ecatalog_order_id' => $shipment->ecatalog_order_id,
            'courier_id' => $shipment->courier_id,
            'date' => now(),
            'address' => $shipment->shipping_address,
        ]);
    }

    public function completedOrder($shipment, array $data)
    {
        return $shipment->update($data);
    }
}
