<?php

namespace App\Repositories;

use App\Interfaces\ProfileRepositoryInterface;
use App\Models\Courier;
use App\Models\CourierActivities;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class ProfileRepository implements ProfileRepositoryInterface
{
    public function getProfile()
    {
        return Courier::where('id', Auth::user()->id)->first();
    }

    public function updatePassword(array $password)
    {
        Courier::where('id', Auth::user()->id)->update($password);
        return Auth::user()->tokens()->delete();
    }

    public function updatePicture(array $image)
    {
        $user = Courier::where('id', Auth::user()->id)->first();
        Storage::delete($user->profile_photo);
        $user->update($image);
    }

    public function updateStatus($status)
    {
        CourierActivities::create([
            'courier_id' => Auth::user()->id,
            'status' => $status,
        ]);

        Courier::where('id', Auth::user()->id)->update(['status' => $status]);
    }
}
