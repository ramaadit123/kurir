<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'email'             => 'required|email|unique:courier,email',
            'name'              => 'required|min:5|max:30',
            'birth_date'        => 'required|date|after:' . now()->subYears(55)->toDateString(),
            'address'           => 'required',
            'phone'             => 'required|numeric|unique:courier,phone',
            'device_id'         => 'required',
            'sim'               => 'required|numeric|unique:courier,sim',
            'nik'               => 'required|digits:15|numeric|unique:courier,nik',
            'year'              => 'required|date_format:Y|before:' . now()->subYears(10)->toDateString(),
            'police_number'     => 'required|unique:courier,police_number',
            'vehicle_name'      => 'required',
            'account_number'    => 'required',
            'account_bank'      => 'required',
            'account_name'      => 'required',
            'profile_photo'     => 'required|image|mimes:png,jpg,jpeg|max:5000',
            'password'          => 'required|min:5',
            'confirm_password'  => 'required|same:password',
            'file_document'     => 'required|mimes:pdf|max:5000',
        ];
    }
}
