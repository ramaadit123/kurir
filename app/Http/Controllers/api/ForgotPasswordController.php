<?php

namespace App\Http\Controllers\api;

use App\Http\Components\Api\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Interfaces\ForgotPasswordRepositoryInterface;

class ForgotPasswordController extends Controller
{
    private ForgotPasswordRepositoryInterface $forgotPasswordRepository;

    public function __construct(ForgotPasswordRepositoryInterface $forgotPasswordRepository)
    {
        $this->forgotPasswordRepository = $forgotPasswordRepository;
    }

    public function forgot(ForgotPasswordRequest $request)
    {
        $token = $this->forgotPasswordRepository->getToken($request->email, $request->token);

        if ($token) {
            $currentTime = time();
            $time = $token->time;

            if ($currentTime >= $time && $time >= $currentTime - (90 + 5)) {
                $this->forgotPasswordRepository->updatePassword($request);
                return ApiResponse::success(null, 'Password Berhasil diubah');
            } else {
                $token->delete();
                return ApiResponse::error('Token telah kadaluarsa', 401);
            }
        };
        return ApiResponse::error('Token atau Email tidak ditemukan', 422);
    }
}
