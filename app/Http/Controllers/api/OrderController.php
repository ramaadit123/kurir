<?php

namespace App\Http\Controllers\api;

use App\Models\Shipment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Components\Api\ApiResponse;
use App\Http\Requests\CompletedOrderRequest;
use App\Http\Resources\CompletedOrderResource;
use App\Http\Resources\DetailOrderResource;
use App\Http\Resources\OrderHistoryResource;
use App\Http\Resources\OrderEarningsResource;
use App\Http\Resources\OrderHistorySingleResource;
use App\Http\Resources\ShipmentResource;
use App\Interfaces\OrderRepositoryInterface;
use App\Models\CancelShipment;

class OrderController extends Controller
{
    private OrderRepositoryInterface $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function index(Request $request)
    {
        $order = $this->orderRepository->getHistory();

        if ($request->has('start_date')) {
            $startDate = $request->input('start_date');
            $order->whereDate('sent_at', '>=', $startDate);
        }
        if ($request->has('end_date')) {
            $endDate = $request->input('end_date');
            $order->whereDate('sent_at', '<=', $endDate);
        }
        if ($request->has('q')) {
            $query = $request->input('q');
            $order->where('number', 'like', '%' . $query . '%')->get();
        }

        if ($order) {
            return ApiResponse::success(OrderHistoryResource::collection($order->get()), 'Data Riwayat Order berhasil ditampilkan');
        }
    }

    public function show(Shipment $shipment)
    {
        return ApiResponse::success(new OrderHistorySingleResource($shipment), 'Data Riwayat Order berhasil ditampilkan');
    }

    public function earnings()
    {
        $order = $this->orderRepository->getHistory();

        if ($order) {
            return ApiResponse::success(new OrderEarningsResource($order->get()), 'Data Riwayat Order berhasil ditampilkan');
        }
    }

    public function acceptOrder(Shipment $shipment)
    {
        $cek_courier = $shipment->courier_id;

        if (!$cek_courier) {
            $this->orderRepository->acceptOrder($shipment);
            return ApiResponse::success(new DetailOrderResource($shipment), 'Order berhasil diterima');
        }
        return ApiResponse::error('Order telah diterima kurir lain', '200');
    }

    public function cancelOrder(Shipment $shipment)
    {
        $cek_data_cancel = CancelShipment::where('courier_id', $shipment->courier_id)->first();

        if (!$cek_data_cancel) {
            $this->orderRepository->cancelOrder($shipment);
            return ApiResponse::success(new DetailOrderResource($shipment), 'Pengajuan Pembatalan berhasil dikirim');
        }
        return ApiResponse::error('Sedang menunggu konfirmasi admin');
    }

    public function completedOrder(Shipment $shipment, CompletedOrderRequest $request)
    {
        $data = [
            'status'            => 'Selesai',
            'received_at'       => now(),
            'reception_note'    => $request->reception_note,
            'reception_image'   => $request->file('reception_image')->store('reception_image'),
        ];
        $this->orderRepository->completedOrder($shipment, $data);
        return ApiResponse::success(new CompletedOrderResource($shipment), 'Bukti Penerimaan Barang Berhasil Dikirim, Terima Kasih.');
    }
}
