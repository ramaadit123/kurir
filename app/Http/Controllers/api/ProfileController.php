<?php

namespace App\Http\Controllers\api;

use App\Http\Components\Api\ApiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Resources\ProfileResource;
use App\Interfaces\ProfileRepositoryInterface;

class ProfileController extends Controller
{
    private ProfileRepositoryInterface $profileRepository;

    public function __construct(ProfileRepositoryInterface $profileRepository)
    {
        $this->profileRepository = $profileRepository;
    }

    public function profile()
    {
        return ApiResponse::success(new ProfileResource($this->profileRepository->getProfile()), 'Data profile berhasil ditemukan');
    }

    public function changePassword(ChangePasswordRequest $request)
    {
        $user = $this->profileRepository->getProfile();

        if (!Hash::check($request->current_password, $user->password)) {
            return ApiResponse::error('Password lama tidak sesuai', 401);
        } else if ($request->current_password === $request->new_password) {
            return ApiResponse::error('Password baru tidak boleh sama dengan password lama', 401);
        }

        $this->profileRepository->updatePassword(['password' => bcrypt($request->new_password)]);

        return ApiResponse::success(null, 'Password berhasil diubah');
    }

    public function changePicture(Request $request)
    {
        $image = $request->file('profile_photo')->store('profile_photo');
        $this->profileRepository->updatePicture(['profile_photo' => $image]);

        return ApiResponse::success(null, 'Photo Profile berhasil diubah');
    }

    public function changeStatus(Request $request)
    {
        $this->profileRepository->updateStatus($request->status);
        return ApiResponse::success(new ProfileResource($this->profileRepository->getProfile()), 'Status berhasil diubah');
    }
}
