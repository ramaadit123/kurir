<?php

namespace App\Http\Controllers\api;

use App\Helpers\Otp;
use App\Http\Components\Api\ApiResponse;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Http\Requests\ActivationAccountRequest;
use App\Interfaces\OtpRepositoryInterface;
use Illuminate\Http\Request;

class ActivationAccountController extends Controller
{
    private OtpRepositoryInterface $otpRepository;

    public function __construct(OtpRepositoryInterface $otpRepository)
    {
        $this->otpRepository = $otpRepository;
    }

    public function sendOtp(Request $request)
    {
        if ($this->otpRepository->getAccountByEmail($request->email)) {

            // SendOtp
            Otp::sendOtp($request->email);
            return ApiResponse::success(['email' => $request->email], 'Kode verifikasi telah dikirim ke : ' . $request->email);
        }

        return ApiResponse::error('Email tidak terdaftar', 401);
    }

    public function verifiedOtp(ActivationAccountRequest $request)
    {
        $user = $this->otpRepository->getAccountByEmail($request->email);
        $otpData = $this->otpRepository->getOtp($request->otp);

        if (!$user) {
            return ApiResponse::error('Email tidak ditemukan', 401);
        }

        if (!$otpData) {
            return ApiResponse::error('Kode tidak valid', 401);
        } else {
            $currentTime = time();
            $time = $otpData->time;

            if ($currentTime >= $time && $time >= $currentTime - (90 + 5)) { // 90 detik
                if ($request->type == 'Verification Account') {
                    $this->otpRepository->updateStatus($request);
                    $otpData->delete();

                    return ApiResponse::success('Akun berhasil diverifikasi');
                } else if ($request->type == 'Forgot Password') {
                    $otpData->delete();
                    $token = bcrypt(Str::slug(Str::random('16')));
                    $this->otpRepository->verifToken($user->email, $token, $time);

                    return ApiResponse::success($token, 'Kode berhasil diverifikasi');
                }
            } else {
                return ApiResponse::error('Kode telah kadaluarsa', 401);
            }
        }
    }
}
