<?php

namespace App\Http\Controllers\api;

use App\Helpers\Random;
use App\Http\Components\Api\ApiResponse;
use App\Http\Controllers\Controller;
use App\Http\Requests\AuthenticateRequest;
use App\Http\Requests\AuthRequest;
use App\Http\Resources\AuthResource;
use App\Interfaces\AuthRepositoryInterface;

class AuthController extends Controller
{
    private AuthRepositoryInterface $authRepository;

    public function __construct(AuthRepositoryInterface $authRepository)
    {
        $this->authRepository = $authRepository;
    }

    public function authenticate(AuthenticateRequest $request)
    {
        $courierDetails = ["email" => $request->email, "password" => $request->password];

        if ($this->authRepository->cekCrenditialsCourier($courierDetails)) {
            return ApiResponse::success(new AuthResource($this->authRepository->getToken()), 'Login Berhasil');
        }

        return ApiResponse::error('Kombinasi Email & Password Salah', 401);
    }

    public function registration(AuthRequest $request)
    {
        $courierDetails = [
            "number" => Random::code('DRV', 'courier', 'number'),
            "email" => $request->email,
            "name" => $request->name,
            "birth_date" => $request->birth_date,
            "address" => $request->address,
            "phone" => $request->phone,
            "device_id" => $request->device_id,
            "sim" => $request->sim,
            "nik" => $request->nik,
            "year" => $request->year,
            "police_number" => $request->police_number,
            "vehicle_name" => $request->vehicle_name,
            "account_number" => $request->account_number,
            "account_bank" => $request->account_bank,
            "account_name" => $request->account_name,
            "profile_photo" => $request->file('profile_photo')->store('profile_photo'),
            "registration_date" => now(),
            "file_document" => $request->file('file_document')->store('file_document'),
            "status" => 0,
            "password" => bcrypt($request->password)
        ];

        return ApiResponse::success(new AuthResource($this->authRepository->registration($courierDetails)), 'Courier Berhasil Didaftarkan');
    }

    public function logout()
    {
        $this->authRepository->logout();

        return ApiResponse::success(null, 'Logout Berhasil');
    }
}
