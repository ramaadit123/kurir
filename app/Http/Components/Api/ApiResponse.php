<?php

namespace App\Http\Components\Api;

class ApiResponse
{
    public static function success($data, $message = '')
    {
        return response()->json([
            'success'   => true,
            'message'   => $message,
            'data'      => $data
        ]);
    }

    public static function error($message, $code = 500)
    {
        return response()->json([
            'success'   => false,
            'message'   => $message,
        ], $code);
    }
}
