<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ShipmentResource extends JsonResource
{
    public function toArray($request)
    {
        return parent::toArray($request);
        // return [
        //     'shipment_id'       => $this->number,
        //     'code'              => $this->ecatalog->code,
        //     'user_id'           => $this->ecatalog->user_id,
        //     'user_name'         => $this->ecatalog->member->name,
        //     'phone_number'      => $this->ecatalog->member->phone_number,
        //     'image'             => $this->ecatalog->member->image,
        //     'cardboard'         => $this->packing->cardboard,
        //     'pickup_address'    => $this->pickup_address,
        //     'shipping_address'  => $this->shipping_address,
        // ];
    }
}
