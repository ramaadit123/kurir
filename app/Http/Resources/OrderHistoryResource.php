<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

Carbon::setLocale('id');

class OrderHistoryResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id_transaction'    => $this->number,
            'courier'           => CourierResource::collection($this->courier),
            'shipping_cost'     => number_format($this->shipping_cost, 0, '.', '.'),
            'status'            => $this->status,
            'date'              => Carbon::parse($this->sent_at)->isoFormat('DD MMM YYYY'),
            'time'              => Carbon::parse($this->sent_at)->format('h:i A'),
        ];
    }
}
