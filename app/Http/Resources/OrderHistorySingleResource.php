<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

Carbon::setLocale('id');

class OrderHistorySingleResource extends JsonResource
{
    public function toArray($request)
    {
        $management_fee = $this->shipping_cost * 0.1;
        $net_income = $this->shipping_cost * 0.9;

        return [
            'id_transaction'    => $this->number,
            'courier'           => CourierResource::collection($this->courier),
            'date'              => Carbon::parse($this->sent_at)->isoFormat('D MMMM YYYY'),
            'pickup_address'    => $this->pickup_address,
            'shipping_address'  => $this->shipping_address,
            'status'            => $this->status,
            'shipping_cost'     => number_format($this->shipping_cost, 0, '.', '.'),
            'management_fee'    => number_format($management_fee, 0, '.', '.'),
            'net_income'        => number_format($net_income, 0, '.', '.')
        ];
    }
}
