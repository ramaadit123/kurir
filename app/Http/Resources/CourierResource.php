<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CourierResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'id'            => $this->id,
            'name'          => $this->name,
            'number'        => $this->number,
            'address'       => $this->address,
            'phone'         => $this->phone,
            'police_number' => $this->police_number,
            'vehicle_name'  => $this->vehicle_name,
            'profile_photo' => $this->profile_photo,
        ];
    }
}
