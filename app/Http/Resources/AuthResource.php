<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class AuthResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            "token"             => $this->token,
            "id"                => $this->id,
            "number"            => $this->number,
            "email"             => $this->email,
            "name"              => $this->name,
            "birth_date"        => $this->birth_date,
            "address"           => $this->address,
            "phone"             => $this->phone,
            "device_id"         => $this->device_id,
            "sim"               => $this->sim,
            "nik"               => $this->nik,
            "year"              => $this->year,
            "police_number"     => $this->police_number,
            "vehicle_name"      => $this->vehicle_name,
            "account_number"    => $this->account_number,
            "account_bank"      => $this->account_bank,
            "account_name"      => $this->account_name,
            "profile_photo"     => $this->profile_photo ? Storage::url($this->profile_photo) : null,
            "registration_date" => $this->registration_date,
            "file_document"     => $this->file_document ? Storage::url($this->file_document) : null,
            "status"            => $this->status,
            "updated_at"        => $this->updated_at,
            "created_at"        => $this->created_at,
        ];
    }
}
