<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class CompletedOrderResource extends JsonResource
{
    public function toArray($request)
    {
        return [
            'status'            => $this->status,
            'received_at'       => $this->received_at,
            'reception_image'   => $this->reception_image ? Storage::url($this->reception_image) : null,
            'reception_note'    => $this->reception_note,
        ];
    }
}
