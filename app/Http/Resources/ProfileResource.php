<?php

namespace App\Http\Resources;

use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ProfileResource extends JsonResource
{
    public function toArray($request)
    {
        $currentDate = Carbon::today();
        $currentMonth = Carbon::now()->format('Y-m');
        $status = [
            'Selesai',
            'Selesai Pengembalian'
        ];

        // Count Net Income By Date
        $shippmentCostToday = Shipment::countShippmentCostToday($this->id, $currentDate, $status);
        $shippmentCostMonth = Shipment::countShippmentCostMonth($this->id, $currentMonth, $status);
        // Count Order By Date
        $orderToday = Shipment::countShippmentOrderDay($this->id, $currentDate, $status);
        $orderMonth = Shipment::countShippmentOrderMonth($this->id, $currentMonth, $status);
        // Count Emergency By Date
        $emergencyDay = Shipment::countShippmentEmergencyDay($this->id, $currentDate, $status);
        $emergencyMonth = Shipment::countShippmentEmergencyMonth($this->id, $currentMonth, $status);

        return [
            'profile' => [
                "id"                => $this->id,
                "number"            => $this->number,
                "email"             => $this->email,
                "name"              => $this->name,
                "birth_date"        => $this->birth_date,
                "address"           => $this->address,
                "phone"             => $this->phone,
                "device_id"         => $this->device_id,
                "sim"               => $this->sim,
                "nik"               => $this->nik,
                "year"              => $this->year,
                "police_number"     => $this->police_number,
                "vehicle_name"      => $this->vehicle_name,
                "account_number"    => $this->account_number,
                "account_bank"      => $this->account_bank,
                "account_name"      => $this->account_name,
                "profile_photo"     => $this->profile_photo ? Storage::url($this->profile_photo) : null,
                "registration_date" => $this->registration_date,
                "file_document"     => $this->file_document ? Storage::url($this->file_document) : null,
                "status"            => $this->status,
                "created_at"        => $this->created_at,
                "updated_at"        => $this->updated_at,
            ],
            'balance' => [
                'income_today'      => number_format($shippmentCostToday, 0, '.', '.'),
                'income_month'      => number_format($shippmentCostMonth, 0, '.', '.'),
                'order_today'       => $orderToday,
                'order_month'       => $orderMonth,
                'emergency_today'   => $emergencyDay,
                'emergency_month'   => $emergencyMonth,
            ]
        ];
    }
}
