<?php

namespace App\Http\Resources;

use App\Models\Shipment;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

Carbon::setLocale('id');

class OrderEarningsResource extends JsonResource
{
    public function toArray($request)
    {
        $status = [
            'Selesai',
            'Selesai Pengembalian'
        ];
        $date = Carbon::createFromFormat('d/m/Y', $request->input('date'));

        // Count Earning By Date
        $day = Shipment::countDayEarning(Auth::user()->id, $date, $status);
        $week = Shipment::countWeekEarning(Auth::user()->id, $date, $status);
        $month = Shipment::countMonthEarning(Auth::user()->id, $date, $status);

        // Count Net Income By Date
        $net_income_day = Shipment::countDayNetIncome(Auth::user()->id, $date, $status);
        $net_income_week = Shipment::countWeekNetIncome(Auth::user()->id, $date, $status);
        $net_income_month = Shipment::countMonthNetIncome(Auth::user()->id, $date, $status);

        return [
            'day' => [
                'date'          => $date->isoFormat('DD MMMM YYYY'),
                'total'         => $day,
                'net_income'    => number_format($net_income_day, 0, '.', '.')
            ],
            'week' => [
                'date'          => $date->isoFormat('DD') . ' - ' . $date->copy()->addDays(7)->isoFormat('DD') . ' ' . $date->isoFormat('MMMM') . ' ' . $date->isoFormat('YYYY'),
                'total'         => $week,
                'net_income'    => number_format($net_income_week, 0, '.', '.')
            ],
            'month' => [
                'date'          => $date->isoFormat('MMMM') . ', ' . $date->isoFormat('YYYY'),
                'total'         => $month,
                'net_income'    => number_format($net_income_month, 0, '.', '.')
            ]
        ];
    }
}
