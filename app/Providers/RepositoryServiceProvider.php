<?php

namespace App\Providers;

use App\Interfaces\AuthRepositoryInterface;
use App\Interfaces\ForgotPasswordRepositoryInterface;
use App\Interfaces\OrderRepositoryInterface;
use App\Interfaces\OtpRepositoryInterface;
use App\Interfaces\ProfileRepositoryInterface;
use App\Repositories\AuthRepository;
use App\Repositories\ForgotPasswordRepository;
use App\Repositories\OrderRepository;
use App\Repositories\OtpRepository;
use App\Repositories\ProfileRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(AuthRepositoryInterface::class, AuthRepository::class);
        $this->app->bind(ProfileRepositoryInterface::class, ProfileRepository::class);
        $this->app->bind(OrderRepositoryInterface::class, OrderRepository::class);
        $this->app->bind(ForgotPasswordRepositoryInterface::class, ForgotPasswordRepository::class);
        $this->app->bind(OtpRepositoryInterface::class, OtpRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
