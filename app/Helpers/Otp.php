<?php

namespace App\Helpers;

use App\Models\EmailVerification;
use Illuminate\Support\Facades\Mail;

class Otp
{
    public static function sendOtp($email)
    {
        // Generate OTP 6 digits
        $otp = rand(100000, 999999);
        $time = time();

        // Save OTP in database Email Verification
        EmailVerification::updateOrCreate(
            ['email' => $email],
            ['email' => $email, 'otp' => $otp, 'time' => $time]
        );

        // Send OTP By Email
        $data['email'] = $email;
        $data['title'] = 'Kode Verifikasi SBW';
        $data['body'] = 'Kode kamu adalah: ' . $otp . ' Kadaluarsa dalam: 90 Detik';

        // Send Message Email
        Mail::send('emails.reset.mailVerification', ['data' => $data], function ($message) use ($data) {
            $message->to($data['email'])->subject($data['title']);
        });
    }
}
