<?php

namespace App\Helpers;

use Illuminate\Support\Facades\DB;

class Random
{
    public static function code($prefix, $table, $field)
    {
        $get = DB::table($table)->select($field)->latest($field)->first();
        if (!$get) {
            return $prefix . '.000000001';
        }
        $latest = $get->$field;

        $number = substr($latest, -9);
        $string = $prefix . '.' . sprintf('%09d', $number + 1);

        return $string;
    }

    public static function code_where($prefix, $table, $field, $where)
    {
        $get = DB::table($table)->select($field)->where([$where])->latest($field)->first();
        if (!$get) {
            return $prefix . '.000000001';
        }
        $latest = $get->$field;

        $number = substr($latest, -9);
        $string = $prefix . '.' . sprintf('%09d', $number + 1);

        return $string;
    }

    public static function code_number($table, $field)
    {
        $get = DB::table($table)->select($field)->latest()->first();
        if (!$get) {
            return '000000001';
        }
        $latest = $get->$field;

        $number = substr($latest, -9);
        $string = sprintf('%09d', $number + 1);

        return $string;
    }
    public static function code_product($prefix, $table, $field)
    {
        $get = DB::table($table)->select($field)->where('code_product', 'like', $prefix . '%')->latest()->orderBy('code_product', 'desc')->first();
        // return $get;
        if (!$get) {
            return $prefix . '.0001';
        }
        $latest = $get->$field;

        $number = substr($latest, -4);
        $string = $prefix . '.' . sprintf('%04d', $number + 1);

        return $string;
    }
}
