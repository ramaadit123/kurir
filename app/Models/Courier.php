<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Courier extends Authenticatable
{
    use HasFactory, HasApiTokens, Notifiable;

    protected $table = 'courier';
    protected $guarded = ['id'];
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function getPictureUrlAttribute()
    {
        return asset('storage/' . $this->picture);
    }

    public function shipments()
    {
        return $this->hasMany(Shipment::class);
    }
}
