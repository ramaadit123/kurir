<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Shipment extends Model
{
    use HasFactory;

    protected $table = 'shipments';
    protected $guarded = [];

    public function courier()
    {
        return $this->belongsToMany(Courier::class, 'shipments', 'id');
    }

    public function ecatalog()
    {
        return $this->belongsTo(ecatalogOrder::class, 'ecatalog_order_id');
    }

    public function packing()
    {
        return $this->belongsTo(Packing::class, 'packing_id');
    }

    public static function countDayEarning($courierId, $date, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereDay('sent_at', $date->day)
            ->whereIn('status', $status)
            ->count();
    }

    public static function countWeekEarning($courierId, $date, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereMonth('sent_at', $date->month)
            ->whereIn('status', $status)
            ->count();
    }

    public static function countMonthEarning($courierId, $date, $status)
    {
        return self::where('courier_id', $courierId)
            ->where('sent_at', '>=', $date)
            ->where('sent_at', '<', $date->copy()->addDays(7))
            ->whereIn('status', $status)
            ->count();
    }

    public static function countDayNetIncome($courierId, $date, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereDay('sent_at', $date->day)
            ->whereIn('status', $status)
            ->sum(DB::raw('shipping_cost * 0.9'));
    }

    public static function countWeekNetIncome($courierId, $date, $status)
    {
        return self::where('courier_id', $courierId)
            ->where('sent_at', '>=', $date)
            ->where('sent_at', '<', $date->copy()->addDays(7))
            ->whereIn('status', $status)
            ->sum(DB::raw('shipping_cost * 0.9'));
    }

    public static function countMonthNetIncome($courierId, $date, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereMonth('sent_at', $date->month)
            ->whereIn('status', $status)
            ->sum(DB::raw('shipping_cost * 0.9'));
    }

    public static function countShippmentCostToday($courierId, $currentDate, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereDate('sent_at', $currentDate)
            ->whereIn('status', $status)
            ->sum(DB::raw('shipping_cost * 0.9'));
    }

    public static function countShippmentCostMonth($courierId, $currentMonth, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereIn('status', $status)
            ->whereRaw('DATE_FORMAT(sent_at, "%Y-%m") = ?', [$currentMonth])
            ->sum(DB::raw('shipping_cost * 0.9'));
    }

    public static function countShippmentOrderDay($courierId, $currentDate, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereDate('sent_at', $currentDate)
            ->whereIn('status', $status)
            ->count();
    }

    public static function countShippmentOrderMonth($courierId, $currentMonth, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereIn('status', $status)
            ->whereRaw('DATE_FORMAT(sent_at, "%Y-%m") = ?', [$currentMonth])
            ->count();
    }

    public static function countShippmentEmergencyDay($courierId, $currentDate, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereDate('sent_at', $currentDate)
            ->whereIn('status', $status)
            ->where('emergency_checked', 1)
            ->count();
    }

    public static function countShippmentEmergencyMonth($courierId, $currentMonth, $status)
    {
        return self::where('courier_id', $courierId)
            ->whereIn('status', $status)
            ->where('emergency_checked', 1)
            ->whereRaw('DATE_FORMAT(sent_at, "%Y-%m") = ?', [$currentMonth])
            ->count();
    }
}
