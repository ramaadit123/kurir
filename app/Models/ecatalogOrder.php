<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ecatalogOrder extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'ecatalog_orders';

    public function member()
    {
        return $this->belongsTo(Member::class, 'user_id');
    }
}
